import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FilmsComponent } from './films/films-component.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ApplicationService } from './service/application.service';
import { HttpService } from './service/http.service';
import {TabViewModule} from 'primeng/tabview';
import { MoreDetailsComponent } from './more-details.component/more-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PeopleComponent } from './people/people.component';
import { PlanetsComponent } from './planets/planets.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    FilmsComponent,
    MoreDetailsComponent,
    PeopleComponent,
    PlanetsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [HttpService, ApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
