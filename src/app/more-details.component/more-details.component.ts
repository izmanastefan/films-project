import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../service/application.service';
import { ActivatedRoute } from '@angular/router';

import { from, Observable, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'more-details',
  templateUrl: './more-details.component.html',
  styleUrls: ['./more-details.component.css'],
})
export class MoreDetailsComponent implements OnInit {
  film: any;
  id: string | null = '';

  constructor(
    private applicationService: ApplicationService,
    private activavatedRouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.activavatedRouter.snapshot.paramMap.get('id');
    if (this.id !== null) {
      this.applicationService.getFilmById$(this.id).subscribe((data) => {
        this.film = data;
        console.log(data)
      });
    }
  }
}
