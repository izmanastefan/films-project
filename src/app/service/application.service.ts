import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable()
export class ApplicationService {
  constructor(private httpService: HttpService) {}

  selectedComponentSubject = new BehaviorSubject<string>('');

  getSelectedComponent$(){
   return this.selectedComponentSubject.asObservable();
  }
  
  setSelectedComponentSubject(value: string){
    this.selectedComponentSubject.next(value);
  }

  getFilms$() {
    return this.httpService.getFilms();
  }

  getFilmById$(id: string | null) {
    return this.httpService.getFilmById(id);
  }

  getPeople$() {
    return this.httpService.getPeople();
  }

  getPlanets$() {
    return this.httpService.getPlanets();
  }

  search$(input: string) {
    return this.httpService.search(input);
  }
}
