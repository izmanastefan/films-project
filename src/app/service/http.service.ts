import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpService {
  constructor(private httpClient: HttpClient) {}

  getFilms(): Observable<any> {
    const url = `https://swapi.dev/api/films`;
    return this.httpClient.get(url);
  }

  getFilmById(id: string | null): Observable<any> {
    const url = `https://swapi.dev/api/films/${id}`;
    return this.httpClient.get(url);
  }

  getPeople(): Observable<any> {
    const url = `https://swapi.dev/api/people`;
    return this.httpClient.get(url);
  }

  getPlanets(): Observable<any> {
    const url = `https://swapi.dev/api/planets`;
    return this.httpClient.get(url);
  }

  search(input: string): Observable<any> {
    const url = ` https://swapi.dev/api/people/?search=${input}`;
    return this.httpClient.get(url);
  }
}
