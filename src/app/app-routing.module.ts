import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ROUTES, Routes } from '@angular/router';
import { FilmsComponent } from './films/films-component.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MoreDetailsComponent } from './more-details.component/more-details.component';
import { ApplicationService } from './service/application.service';
import { map } from 'rxjs';

const routes: Routes = [
  { path: 'films', component: DashboardComponent },
  { path: 'films/:id', component: MoreDetailsComponent}
];


@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { 
  
}

export function configFilmsRoutes(this: any, applicationService :ApplicationService){
  let routes: Routes[];

  this.applicationService.getSelectedComponent$().pipe(map((data)=> {
    console.log(data)
  })).subscribe();
}
