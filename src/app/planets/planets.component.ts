import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../service/application.service';
import { ActivatedRoute } from '@angular/router';

import { from, Observable, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css'],
})
export class PlanetsComponent implements OnInit {
  planets: any;
  title: string = '';

  constructor(private applicationService: ApplicationService) {}

  ngOnInit(): void {
    this.applicationService.getPlanets$().subscribe((data) => {
      this.planets = data.results;
    });
  }

  getIdFromURL(url: string) {
    return url.charAt(url.length - 2);
  }
}
