import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { from, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { ApplicationService } from '../service/application.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit{
  constructor(private applicationService: ApplicationService) {}

  result: [] = [];
  searchForm = new FormGroup({
    value: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    this.applicationService.getFilms$().subscribe((data) => {
      this.result = data.results;
    });
  }

  search() {
    this.applicationService.search$(this.searchForm.controls['value'].value).subscribe();
    if (this.searchForm.valid) {
      this.applicationService.getFilms$().subscribe((data) => {
        if (data) {
          this.result = data.results.filter((result: any) =>
            result.title
              .toLowerCase()
              .includes(this.searchForm.controls['value'].value.toLowerCase())
          );
        }
      });
    }
  }
}
