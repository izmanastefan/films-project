import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../service/application.service';
import { ActivatedRoute } from '@angular/router';

import { from, Observable, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css'],
})
export class PeopleComponent implements OnInit {
  people: any;
  title: string = '';

  constructor(private applicationService: ApplicationService) {}

  ngOnInit(): void {
    this.applicationService.getPeople$().subscribe((data) => {
      this.people = data.results;
    });
  }

  getIdFromURL(url: string) {
    return url.charAt(url.length - 2);
  }
}
