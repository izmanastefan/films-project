import { Component, Input, OnInit } from '@angular/core';
import { ApplicationService } from '../service/application.service';

import { from, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'films',
  templateUrl: './films-component.component.html',
  styleUrls: ['./films-component.component.css'],
})
export class FilmsComponent{
  @Input()
  public searchedFilms: any;

  films: any;
  title: string = '';

  constructor(private applicationService: ApplicationService) {}

  getIdFromURL(url: string) {
    return url.charAt(url.length - 2);
  }
}
